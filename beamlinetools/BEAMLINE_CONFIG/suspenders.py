from .base import *
from .beamline import *
from bluesky.suspenders import SuspendFloor
from beamlinetools.reworked_suspenders.suspenders_bessy import SuspendFloor


# this suspends the plans 10 seconds before the injection, and it resumes once the injection is over.

# sus = SuspendFloor(next_injection.time_next_injection, 10, resume_thresh=11)
# RE.install_suspender(sus)